#include <cmath>
#include <gtest/gtest.h>

#include "Calculator.h"

class CalculatorTest : public ::testing::Test {
protected:
    Calculator m_calculator;
};

TEST_F(CalculatorTest, subtraction)
{
    EXPECT_DOUBLE_EQ(2.0, m_calculator.calculate("1+1"));
}

TEST_F(CalculatorTest, addition)
{
    EXPECT_DOUBLE_EQ(1.0, m_calculator.calculate("2-1"));
}

TEST_F(CalculatorTest, addition_substraction)
{
    EXPECT_DOUBLE_EQ(8.8, m_calculator.calculate("2-1+4.5+3.3"));
}

TEST_F(CalculatorTest, minus_number)
{
    EXPECT_DOUBLE_EQ(-134.2, m_calculator.calculate("-134.2"));
}

TEST_F(CalculatorTest, multiplication)
{
    EXPECT_DOUBLE_EQ(-603.9, m_calculator.calculate("-134.2*4.5"));
}

TEST_F(CalculatorTest, division)
{
    EXPECT_DOUBLE_EQ(-67.1, m_calculator.calculate("-134.2/2"));
}

TEST_F(CalculatorTest, division_by_zero)
{
    EXPECT_DOUBLE_EQ(-std::numeric_limits<double>::infinity(), m_calculator.calculate("-134.2/0"));
}

TEST_F(CalculatorTest, division_zero_by_zero)
{
    EXPECT_TRUE(std::isnan(m_calculator.calculate("0/0")));
}

TEST_F(CalculatorTest, priorities)
{
    EXPECT_DOUBLE_EQ(5.0, m_calculator.calculate("-1+2*3"));
}

TEST_F(CalculatorTest, parenthesis)
{
    EXPECT_DOUBLE_EQ(3.0, m_calculator.calculate("(-1+2)*3"));
}

TEST_F(CalculatorTest, parenthesis_and_priorities)
{
    EXPECT_DOUBLE_EQ(-1.0, m_calculator.calculate("(-1+2)*3+(-4)"));
}
