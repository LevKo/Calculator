#include <iomanip>
#include <iostream>

#include "Calculator.h"

using namespace std;

int main() {
    Calculator calculator;
    string epression;
    cin >> epression;
    cout << calculator.calculate(epression) << endl;
    return 0;
}
